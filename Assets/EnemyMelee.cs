﻿using UnityEngine;
using System.Collections;

public class EnemyMelee : Enemy
{
    void Start()
    {
        _target = GameObject.FindGameObjectWithTag("Player");
        if(_target.tag == "Player")
        {
            _playerStats = _target.GetComponent<PlayerStats>();
        }

        Health = 75;
    }

    void Update()
    {
        if(Range() > 250f)
        {
            Idle();
        }
        else if (Range() < 250f && Range() > 1f)
        {
            Chasing();
        }

        if(Health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    void Idle()
    {

    }

    void Chasing()
    {
        transform.position = Vector3.MoveTowards(transform.position, _target.transform.position, MoveSpeed);
    }

    void OnCollisionEnter(Collision col)
    {
        // damage code here
        if (col.transform.tag == "Player")
        {
            _playerStats.Health -= 15;
        }
    }
}
