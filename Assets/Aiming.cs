﻿using UnityEngine;
using System.Collections;

public class Aiming : MonoBehaviour
{

    public GameObject _player;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        AimAtMouse();
    }

    void AimAtMouse()
    {
        Vector3 mouse = Input.mousePosition;
        mouse.z = Mathf.Abs(Camera.main.transform.position.y - transform.position.y);
        mouse = Camera.main.ScreenToWorldPoint(mouse);
        transform.LookAt(mouse);
    }


}
