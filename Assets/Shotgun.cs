﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Shotgun : Gun, IGun
{

    
    private bool _shot;

    void Start()
    {
        totalAmmo = 30;
        fireTimer = 0f;
        rateOfFire = 1f;
        magazineSize = 6;
        currentAmmo = magazineSize;
        baseDamage = 15;
        weight = 0;
        reliability = 1;
        accuracy = 0.075f;

        _bulletPrefab = Resources.Load("Bullet") as GameObject;
    }

    public void Shoot()
    {
        // Can only be shot once per click, so no need for timer
        // But timer between shots so can't be spammed
        // Get direction and normalize it
        Vector3 fireDirection = AimDirection();
        fireDirection.Normalize();
        // Loop to instantiate each pellet with accuracy sway
        if (Input.GetButtonDown("Fire1") && currentAmmo > 0 && !_shot)
        {
            for (int i = 0; i < 6; i++)
            {
                float force = 10f + Random.Range(-1.0f, 1.0f);
                if (i > 1)
                {
                    fireDirection.x += Random.Range(-accuracy, accuracy);
                    fireDirection.z += Random.Range(-accuracy, accuracy);
                }
                _bullet = Instantiate(_bulletPrefab, transform.position, Quaternion.identity) as GameObject;
                SetDamage(_bullet);
                _bullet.GetComponent<Rigidbody>().AddForce(fireDirection.normalized * force, ForceMode.Impulse);
                
            }
            currentAmmo--;
            _shot = true;
        }
    }

    public void Reload()
    {

    }

    public bool IsLoaded()
    {
        return true;
    }

    void ShotTimer()
    {
        Image loadingImage = GameObject.FindGameObjectWithTag("LoadingImage").GetComponent<Image>();
        if(_shot)
        {
            fireTimer += Time.deltaTime;
            if(fireTimer >= rateOfFire)
            {
                _shot = false;
                fireTimer = 0f;
            }
        }
        loadingImage.fillAmount = fireTimer / rateOfFire;
    }

    void Update()
    {
        ShotTimer();
    }

}
