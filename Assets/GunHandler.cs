﻿using UnityEngine;
using System.Collections;

public class GunHandler : MonoBehaviour
{
    public IGun gunType;
    private bool _reloading;

    void Awake()
    {
        SetGun(this.gameObject.AddComponent<Pistol>());
    }

    // Use this for initialization
    void Start()
    {
        //SetGun(this.gameObject.AddComponent<Pistol>());
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButton("Fire1"))
        {
            gunType.Shoot();
        }
        if (Input.GetButton("Fire2") && !_reloading)
        {
            _reloading = true;
        }
        if (_reloading)
        {
            gunType.Reload();
        }
        if (gunType.IsLoaded())
        {
            _reloading = false;
        }
    }

    public void SetGun(IGun _gun)
    {
        gunType = _gun;
    }
}
