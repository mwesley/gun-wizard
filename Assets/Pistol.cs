﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Pistol : Gun, IGun
{

    private GameObject _bullet;
    private GameObject BulletPrefab;
    private bool _quickReloadFail;

    void Start()
    {
        totalAmmo = 60;
        fireTimer = 0f;
        rateOfFire = 0.15f;
        magazineSize = 12;
        currentAmmo = magazineSize;
        reloadTime = 2f;
        currentReloadTime = 0;
        baseDamage = 15;
        weight = 0;
        reliability = 1;
        accuracy = 0.05f;
        _quickReloadFail = false;

        BulletPrefab = Resources.Load("Bullet") as GameObject;
        InvokeRepeating("RegenerateAmmo", 0, 1);
    }

    public void Shoot()
    {
        // Progress fire timer
        fireTimer += Time.deltaTime;
        // Check if fire timer is greater than or equal to rate of fire
        // and if current ammo loaded into gun is more than 0
        // and that the gun is not being reloaded
        if (fireTimer >= rateOfFire && currentAmmo > 0 && !_reloading)
        {
            // Get direction and then normalize it
            Vector3 fireDirection = AimDirection();
            fireDirection.Normalize();
            // Add accuracy sway
            fireDirection.x += Random.Range(-accuracy, accuracy);
            fireDirection.z += Random.Range(-accuracy, accuracy);
            // Instantiate the bullet and give it velocity
            _bullet = Instantiate(BulletPrefab, transform.position, Quaternion.identity) as GameObject;
            SetDamage(_bullet);
            _bullet.GetComponent<Rigidbody>().AddForce(fireDirection.normalized * 10f, ForceMode.Impulse);
            // Reset fire timer and reduce loaded ammo
            fireTimer = 0f;
            currentAmmo--;
        }
    }

    public void Reload()
    {
        _reloading = true;
        GameObject reloadObj = GameObject.FindGameObjectWithTag("Reload");
        reloadImage = reloadObj.GetComponent<Image>();

        // Check if gun is fully loaded before reloading
        if (IsLoaded())
        {
            _reloading = false;
            return;
        }

        // Get amount of ammo to be loaded into gun, then load into it gun
        float ammoToBeLoaded = magazineSize - currentAmmo;
        if (ammoToBeLoaded > totalAmmo)
        {
            ammoToBeLoaded = totalAmmo;
        }

        // Progress timer towards the time it takes to to reload
        currentReloadTime += Time.deltaTime;
        reloadImage.fillAmount = currentReloadTime / reloadTime;

        // After reload time is reached, stop and fill gun with ammo
        if (currentReloadTime >= reloadTime)
        {
            currentAmmo += ammoToBeLoaded;
            // Then minus it from total ammo;
            totalAmmo -= ammoToBeLoaded;
            currentReloadTime = 0;
            _reloading = false;
        }

        // Code for an active reload mechanic, if you hit reload again during the time frame, you reload quicker than if you waited it out.
        // If you miss the window, reloading takes longer
        if (Input.GetButtonUp("Fire2") && !_quickReloadFail)
        {
            if (currentReloadTime <= (reloadTime / 2) + 0.125f * reliability && currentReloadTime >= (reloadTime / 2) - 0.125f * reliability && !_quickReloadFail)
            {
                currentAmmo += ammoToBeLoaded;
                // Then minus it from total ammo;
                totalAmmo -= ammoToBeLoaded;
                currentReloadTime = 0;
                reloadImage.fillAmount = 1;
                _reloading = false;
            }
            else
            {
                currentReloadTime -= 0.5f;
                _quickReloadFail = true;
            }
        }

    }

    public bool IsLoaded()
    {
        // Check if total amount of ammo is 0, as if it is, the gun is fully loaded with all available ammo
        if (totalAmmo == 0)
        {
            _quickReloadFail = false;
            return true;
        } // Then check if current ammo is equal to the mag size of the gun
        else if (currentAmmo == magazineSize)
        {
            _quickReloadFail = false;
            return true;
        } // If neither of these are true, the gun is not fully loaded
        return false;
    }

    void RegenerateAmmo()
    {
        if (totalAmmo <= 59)
        {
            totalAmmo++;
        }
    }


}
