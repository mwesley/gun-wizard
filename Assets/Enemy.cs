﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    protected GameObject _target;           // target of enemy
    protected PlayerStats _playerStats;     // players stats script
    public float MoveSpeed;                 // speed that enemy can move

    public int Health;

    void Start()
    {

    }

    protected float Range()
    {
        Vector3 range = new Vector3(_target.transform.position.x - this.transform.position.x, _target.transform.position.y - this.transform.position.y, _target.transform.position.z - this.transform.position.z);
        return range.sqrMagnitude;
    }
}
