﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Gun : MonoBehaviour {

    protected float rateOfFire;
    protected float fireTimer;
    protected float magazineSize;
    public float currentAmmo;
    public float reloadTime;
    protected float currentReloadTime;
    protected int baseDamage;
    protected float weight;
    protected float reliability;
    protected float accuracy;
    public bool _reloading;
    public float totalAmmo;

    protected GameObject _bullet;
    protected GameObject _bulletPrefab;

    protected Image reloadImage;

    void Start()
    {
    }

	protected Vector3 AimDirection()
    {
        Vector3 mouse = Input.mousePosition;
        mouse.z = Mathf.Abs(Camera.main.transform.position.y - transform.position.y);
        mouse = Camera.main.ScreenToWorldPoint(mouse);
        Vector3 dir = new Vector3(mouse.x - transform.position.x, 0, mouse.z - transform.position.z);
        return dir;
    }

    protected void SetDamage(GameObject bullet)
    {
        bullet.GetComponent<BulletScript>().Damage = baseDamage;
    }

}
