﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour
{
    public int Health;      // Players health
    public int Mana;        // Players mana for casting spells

    // Use this for initialization
    void Start()
    {
        Health = 100;
        Mana = 100;
    }

}
