﻿using UnityEngine;
using System.Collections;

public class PlayerAnimController : MonoBehaviour
{

    Animator animator;
    Rigidbody playerRigidbody;

    //flags to check animations
    bool _isPlaying_moving;
    bool _isPlayer_idle;

    //animation states
    const int STATE_IDLE = 0;
    const int STATE_MOVING = 1;

    // Use this for initialization
    void Start()
    {
        animator = this.GetComponent<Animator>();
        playerRigidbody = this.transform.parent.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float speed = playerRigidbody.velocity.x;

        animator.SetFloat("speed", speed);
    }
}
