﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour
{
    public int Damage;

    void OnCollisionEnter(Collision col)
    {
        if(col.transform.tag == "Enemy")
        {
            col.gameObject.GetComponent<Enemy>().Health -= Damage;
        }
        Debug.Log(col.transform.name);
        Destroy(this.gameObject);
    }
}
