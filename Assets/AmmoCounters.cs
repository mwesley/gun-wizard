﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AmmoCounters : MonoBehaviour
{

    private Text _totalAmmo;
    private Text _currentAmmo;

    private Gun _currentGun;

    // Use this for initialization
    void Start()
    {
        _totalAmmo = GameObject.FindGameObjectWithTag("TotalAmmo").GetComponent<Text>();
        _currentAmmo = GameObject.FindGameObjectWithTag("CurrentAmmo").GetComponent<Text>();

        _currentGun = GameObject.FindGameObjectWithTag("PlayerGun").GetComponent<Gun>();
    }

    // Update is called once per frame
    void Update()
    {
        _totalAmmo.text = "" + _currentGun.totalAmmo;
        _currentAmmo.text = "" + _currentGun.currentAmmo;
    }
}
