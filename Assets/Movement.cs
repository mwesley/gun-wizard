﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    public float Speed;

    private Rigidbody _playerRigidbody;

    // Use this for initialization
    void Start()
    {
        _playerRigidbody = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement();
    }

    void PlayerMovement()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        _playerRigidbody.AddForce(new Vector3(horizontal, 0, vertical) * Speed, ForceMode.Impulse);
    }
}
